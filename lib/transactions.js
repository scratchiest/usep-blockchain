'use strict';

/**
 * Invoke update wallet transaction
 * @param {cashless.transactions.UpdateWallet} update - the update to be processed
 * @transaction
 */
async function updateWallet(update) {

    let assetRegistry = await getAssetRegistry('cashless.assets.Wallet');
    let wallet = update.person.wallet;
    var operators = {
        'ADD': function(oldValue, newValue) { return oldValue + newValue },
        'DEDUCT': function(oldValue, newValue) { return oldValue - newValue },
    };
    
    wallet.value = operators[update.action](wallet.value, update.value);
    
    if(wallet.value > 2000){
        throw new Error('Wallet limit is 2000!');
    }

    await assetRegistry.update(wallet);
}

/**
 * Invoke create person transaction
 * @param {cashless.transactions.CreatePerson} create - the create to be processed
 * @transaction
 */
async function createPerson(create) {

    // Get the factory.
    var factory = getFactory();
    let assetRegistry = await getAssetRegistry('cashless.assets.Wallet');
    let participantRegistry = await getParticipantRegistry('cashless.participants.Person');

    var id = Date.now().toString(36) + Math.random().toString(36).substr(2, 9);

    // Create a new person
    var person = factory.newResource('cashless.participants', 'Person', 'PERSON_' + id);
    // Create a new wallet
    var wallet = factory.newResource('cashless.assets', 'Wallet', 'WALLET_' + id);

    // insert person data
    person.firstName = create.firstName;
    person.middleName = create.middleName;
    person.lastName = create.lastName;
    person.gender = create.gender;
    person.type = (typeof create.type === 'undefined') ? 'STANDARD' : create.type;
    person.wallet = wallet;

    // insert wallet data
    wallet.owner = person;

    // persist the state of the generated asset and participant
    await participantRegistry.add(person);
    await assetRegistry.add(wallet);
}

/**
 * Invoke selling transaction
 * @param {cashless.transactions.Sell} sell - the selling to be processed
 * @transaction
 */
async function sell(sell) {

    if(sell.seller.type != 'VENDOR'){
        throw new Error('Seller is not authorized!');
    }

    let assetRegistry = await getAssetRegistry('cashless.assets.Wallet');
    let buyerWallet = sell.buyer.wallet;
    let sellerWallet = sell.seller.wallet;
    
    buyerWallet.value = buyerWallet.value - sell.value;
    sellerWallet.value = sellerWallet.value + sell.value;
    
    if(buyerWallet.value < 0){
        throw new Error('Insufficient balance!');
    }

    await assetRegistry.update(buyerWallet);
    await assetRegistry.update(sellerWallet);
}